import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Login from './src/components/loginComponent'
import Dashboard from './src/components/dashboardComponent'
import {Provider} from 'react-redux'
import store from './src/redux/store/store'
import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import NavigationService from './src/services/navigationService'
import FlashMessage from 'react-native-flash-message'


const MainNavigator = createStackNavigator({
  login: {screen: Login},
  dashboard: {screen: Dashboard}
});

const AppContainer = createAppContainer(MainNavigator);

export default class App extends React.Component {

  render()
  {
    return (
      
      <Provider store={store}>
          <AppContainer
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
          <FlashMessage position="top" />
      </Provider>

  );
  }
  
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
