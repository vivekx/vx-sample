const initialState = {
    employees: [],
    error: undefined
}

export default function employeesReducer(state = initialState, action) {
    console.log(action)
    switch(action.type) {
        case 'FETCH_EMPLOYEE_SUCCESS':
            return {
                ...state,
                pending: false,
                employees: action.employees
            }
        case 'FETCH_EMPLOYEE_ERROR':
            return {
                ...state,
                pending: false,
                error: action.error
            }
        default: 
            return state;
    }
}
